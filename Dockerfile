FROM selenium/standalone-chrome

MAINTAINER Heng ZENG <heng.zeng@globalis-ms.com>

# disable interactive functions
ENV DEBIAN_FRONTEND noninteractive

USER root

RUN apt-get -qqy update && \

    apt-get -qqy upgrade && \

    # install neede packages
    apt-get install  -y --no-install-recommends apt-utils ca-certificates && \

    # upgrade OS
    apt-get -y dist-upgrade && \

    apt-get autoremove -y && \

    # remove apt cache from image
    apt-get clean all

RUN apt-get install -y --no-install-recommends wget curl zip unzip patch less nano psmisc git

#WKHTMLTOPDf
RUN apt-get install -y xvfb libfontconfig wkhtmltopdf

#APACHE
RUN apt-get install -y apache2

#MySQL
RUN echo 'mysql-server mysql-server/root_password password password' | debconf-set-selections
RUN echo 'mysql-server mysql-server/root_password_again password password' | debconf-set-selections
RUN apt-get install -y mysql-server


#PHP
RUN apt-get install -y php libapache2-mod-php  \
    php-fpm \
    php-cli \
    php-common \
    php-mysql \
    php-pgsql \
    php-sqlite3 \
    php-pdo-sqlite \
    php-pear \
    php-redis \
    php-memcache \
    php-memcached \
    php-apcu \
    php-soap \
    php-xml \
    php-xmlrpc \
    php-intl \
    php-imagick \
    php-mcrypt \
    php-mbstring \
    php-bz2 \
    php-json \
    php-gd \
    php-curl \
    php-imap \
    php-ldap \
    php-gettext \
    php-bcmath \
    php-gmp \
    php-xdebug \
    php-zip \
    php-mcrypt && \
    rm -rf /var/lib/apt/lists/*


#Install wp-cli
RUN curl -O https://raw.githubusercontent.com/wp-cli/builds/gh-pages/phar/wp-cli.phar
RUN chmod +x wp-cli.phar
RUN mv wp-cli.phar /usr/local/bin/wp

#Dependencies for Wordpress projects
RUN apt update && apt-get install npm -yq
RUN ln -s /usr/bin/nodejs /usr/bin/node

# Composer
RUN cd /tmp && curl -sS https://getcomposer.org/installer | php && mv composer.phar /usr/local/bin/composer

COPY data/. /

COPY key/auth.json /root/.composer/auth.json
COPY config/. /root/ 

RUN /usr/sbin/a2dissite '000-default.conf' && \
    /usr/sbin/a2ensite 'docker.conf' && \
    /usr/sbin/a2enmod rewrite

WORKDIR /var/www/html/

RUN rm index.html

EXPOSE 80

CMD ["/usr/sbin/apache2ctl", "-D", "FOREGROUND"]

ENTRYPOINT service mysql start && /etc/init.d/apache2 restart  && /bin/bash 
